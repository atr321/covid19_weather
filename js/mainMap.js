const mymap = L.map('mapid',
    {
        minZoom: 3,


        maxBounds: L.latLngBounds([
            [-42.176612, -101.773638],
            [25.296981, -17.250198]
        ]),
        // maxBoundsViscosity: 1.0,
    }

);


const accessToken = "pk.eyJ1Ijoibm9iYXZpIiwiYSI6ImNrYWU5N240cTAyNWEycWxxYms5aWU4bjIifQ.qGzSRSo8vhDXVTW3aU0Nyg";
const info = L.control();

info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info');
    this.update();
    return this._div;
};


info.update = function (props) {
    this._div.innerHTML = '<h4>Brazil Coronavirus Weather</h4>' + (props ?
        '<b>' + props.name + " - " + props.sigla + '</b><br />' + props.density + ' cases<sup>2</sup>'
        : 'Hover over a state');
};

info.addTo(mymap);


function getColor(d) {
    return d > 42 ? 'rgba(128, 0, 38, 0.5)'/* #340000 */ :
    d > 40 ? 'rgba(189, 0, 38, 0.5)'/* #770000 */ :
    d > 38 ? 'rgba(189, 0, 38, 0.5)'/* #b33636 */ :
    d > 36 ? 'rgba(189, 0, 38, 0.5)'/* #c83d3d */ :
    d > 34 ? 'rgba(189, 0, 38, 0.5)'/* #fe4e4e */ :
    d > 32 ? 'rgba(189, 0, 38, 0.5)'/* #fe8989 */ :
    d > 30 ? 'rgba(189, 0, 38, 0.5)'/* #febbbb */ :
    d > 28 ? 'rgba(189, 0, 38, 0.5)'/* #d74700 */ :
    d > 26 ? 'rgba(189, 0, 38, 0.5)'/* #ff7220 */ :
    d > 24 ? 'rgba(189, 0, 38, 0.5)'/* #ffa759 */ :
    d > 22 ? 'rgba(189, 0, 38, 0.5)'/* #ffd147 */ :
    d > 20 ? 'rgba(189, 0, 38, 0.5)'/* #f4ea99 */ :
    d > 18 ? 'rgba(189, 0, 38, 0.5)'/* #fcffd8 */ :
    d > 16 ? 'rgba(189, 0, 38, 0.5)'/* #befff5 */ :
    d > 14 ? 'rgba(189, 0, 38, 0.5)'/* #6bb9b9 */ :
    d > 12 ? 'rgba(189, 0, 38, 0.5)'/* #64a0dd */ :
    d > 10 ? 'rgba(189, 0, 38, 0.5)'/* #4b79a7 */ :
    d > 8 ? 'rgba(189, 0, 38, 0.5)'/* #7e93c8 */ :
    d > 6 ? 'rgba(189, 0, 38, 0.5)'/* #b1ade9 */ :
    d > 4 ? 'rgba(189, 0, 38, 0.5)'/* #9592c4 */ :
    d > 2 ? 'rgba(227, 26, 28, 0.5)'/* '#9592c4' */ :
    d > 0 ? 'rgb(252, 78, 42, 42)' /* '#79769f' */ :
    d > -2 ? 'rgba(253, 141, 60, 0.5)' /* #605d7e */ :
    d > -4 ? 'rgba(254, 178, 76, 0.5)'/* '#46445d' */ :
    d > -6 ? 'rgba(254, 217, 118, 0.5)' /* '#23226b' */ :
    'rgba(255, 237, 160, 0.5)'/* '#000079' */;
}

function geoJsonStyler(feature) {
    return {
        fillColor: getColor(feature),
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7
    };
}


function highlightFeature(e) {
    const layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }
}

function mouseOverStateHandler(e) {
    const layer = e.target;
    highlightFeature(e);
    info.update(layer.feature.properties);
}


function mouseOutOfStateHandler(e) {
    resetHighlight(e);
    info.update();
}

function resetHighlight(e) {
    geojson.resetStyle(e.target);
}

function zoomToFeature(e) {
    mymap.fitBounds(e.target.getBounds());
}

function featureHandler(_feature, layer) {
    layer.on({
        mouseover: mouseOverStateHandler,
        mouseout: mouseOutOfStateHandler,
        click: zoomToFeature
    });
}

function getCapitalsWeatherByDate(path){
    const URL = `https://apitempo.inmet.gov.br/condicao/capitais/${path}`;

    fetch(URL,{mode:"cors"})
    .then(data=>{
        return data.json()
    })
    .then(res=>{
        console.log(res)
    })
}



L.tileLayer(`https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=${accessToken}`, {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    // maxZoom: 18,
    // zoom: 4,
    // minZoom: 5,
    id: 'mapbox/light-v9',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: accessToken
}).addTo(mymap);

const geojson = L.geoJson(brazilStates, {
    onEachFeature: featureHandler
}).addTo(mymap);

mymap.setView([-14.235, -51.9253], 4);

-33.533357, -57.326752